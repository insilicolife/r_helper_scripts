#!/bin/bash

## Description:
##   The idea of this script is to:
##   1. list all the available installed versions of R and let the user choose
##      the one they want to be the default.
##   2. help user install the available R versions without hassle


#-------[ initial settings ]-------#
## remember to end folder paths with /
{
    # the full path to the locations that R versions are installed
    init_installation_path='/opt/R/'

    # the full path to the file/symlink that defines the default version of R
    init_default_read_path='/usr/bin/R'

    # the name of tmux session for when the code creates one(e.g during installation of a new version
    init_tmux_session_name='Rinstallation'

    # define some colors for different messages
    init_color_success='\e[92m'
    init_color_error='\e[91m'
    init_color_message='\e[33m'
    init_color_defult='\e[0m'
    
    # define the path that the R source file should be downloaded from
    #init_R_source_repo_path='https://cloud.r-project.org/src/base/R-3/'
    init_R_source_repo_path='https://cloud.r-project.org/src/base/R-4/'
    
    # define a path to the tmp folder. This will be used to store temprorary files
    init_tmp_path='/tmp/'
}


#-------[ internal functions ]-------#
{
    function func_manual () {
        ## Description:
        ##  This function shows the help/manual of this file
        
        echo "----------------------------------------------------------------------"
        echo "Description:"
        echo "    A script to install and manage the installed R version, and"
        echo "    setting the default version."
        echo
        echo "Usage:"
        echo " bash manage.R.version.sh {arg1}"
        echo
        echo "Arguments:"
        echo "  arg1 (Required):"
        echo "   change      To change the defult R version"
        echo "   check       To check what is the default version at the moment"
        echo "   l, list     To list all the installed versions"
        echo "   i, install  To install a new version"
        echo "   remove      To remove (uninstall) a version"
        echo "   h, help     Show this help"
        echo "----------------------------------------------------------------------"
    }


    function func_msg () {
        ## Description:
        ##  It produce color-coded and indented text to the user of the code
        ## 
        ## Arguments:
        ##  - first argument:  is the number of indentation. Think about it as
        ##                      the level number you want the message to be
        ##                      shown.
        ##  - second argument: defines the color of the shown message.
        ##  - third argument:  the string you want to show.
        
        case "${2}" in
            m|message)
                color=${init_color_message}
                ;;
            e|error)
                color=${init_color_error}
                ;;
            s|success)
                color=${init_color_success}
                ;;
            *)
                color=${init_color_defult}
                ;;
        esac
        
        times=${1}
        
        ## create as manu indentation as the user have asked for
        #indent=$(for i in {1..${times}}; do echo -n '|_ '; done)
        #indent="$(yes '|_ ' | head -n ${times})"
        #indent=$(printf "%${times}s" | sed 's/ /|_ /g')
        indent=$(for (( c=1; c<${times}; c++)) ; do echo -n "|  " ; done)
        
        #echo -e "|_ ${color}${3}${init_color_defult}"
        echo -e "${indent}|_ ${color}${3}${init_color_defult}"
    }
    
    
    function func_choice_of () {
        ## Description:
        ##  This function reads a list of items as input, ask the user to
        ##   choose and set a global variable names selected_item with the
        ##   choice of the user.
        ## 
        ## Arguments:
        ##  A series of items to be asked form user
        ## 
        ## Output:
        ##  Does not return anything, but instead it sets a global variable
        ##  named selected_item with the item string that the user have chosen
        
        selected_item="NULL"

        select selected_item
        # [in list] omitted, so 'select' uses arguments passed to function.
        do
            if [ ! -z "${selected_item}" ]; then
                func_msg 2 message "You chose: ${selected_item}"
                break
            else
                func_msg 2 error 'Not a valid choice. You should type the number on the left side of what you want to choose.'
            fi
        done
    }
    
    
    function func_change_version () {
        ## Description:
        ##  This function reads a list installed versions, asks the user what
        ##  they want to set as default and will set that by symlinking the
        ##  /usr/bin/R
        ## 
        ## Arguments:
        ##  A series of items to be asked form user
        ## 
        ## Output:
        ##  Does not return anything

        #-------[ check the requirements ]-------#
        {
            # make sure the user has sudo access
            if [ "$(func_has_sudo)" == "no_sudo" ]; then
                func_msg 1 error 'You need to have sudo access to install R'
                echo
                exit 1
            fi


            # make sure the init_installation_path is a directory and exists
            if [ ! -d ${init_installation_path} ]; then
                func_msg 1 error "The folder ${init_installation_path} that is suppose to contain installed versions of R does not exist!\n   Try using \"install\" argument to install the desired version of R through this script."
                echo
                exit 1
            fi
            
            
            # make sure that some R version are already installed
            if [ $(ls -1 ${init_installation_path} | wc -l) -eq 0 ]; then
                func_msg 1 error "No R version have been installed in ${init_installation_path} \n   Use the \"install\" argument to install your desired version."
                echo
                exit 1
            fi
            
            
            # make sure the init_default_read_path is a symlink
            # if the file exists but it is not a symlink
            if [ ! -L ${init_default_read_path} ] && [ -f ${init_default_read_path} ]; then
                func_msg 1 error "The file ${init_default_read_path} is not a symlink!\n   Perhaps you have already installed R by other means.\n   Remove that and try installing through this script!"
                echo
                exit 1
            fi
        }

        # capture the argument
        local args=$1

        # if no argument was passed to this function
        if [ -z ${args} ]; then
            # get the list of installed R versions
            installed_versions=$(ls ${init_installation_path})
            
            # ask the user what version they want to set as default
            func_msg 1 message 'Select the R version which you want to be the default on your system by entering the index number:'
            func_choice_of ${installed_versions}

            local new_default_version="${selected_item}"
        else
            local new_default_version="${args}"
        fi
        
        echo "|"
        func_msg 1 message "The following settings will be applied:"
        func_msg 2 message "${init_default_read_path} ----> $(func_path_cleaner "${init_installation_path}${new_default_version}/bin/R")"
        func_msg 2 message "${init_default_read_path}script ----> $(func_path_cleaner "${init_installation_path}${new_default_version}/bin/Rscript")"
        echo "|"

        # set the selected version as the default in form of a symlink
        sudo ln -sf  "$(func_path_cleaner "${init_installation_path}${new_default_version}/bin/R")" ${init_default_read_path}
        sudo ln -sf  "$(func_path_cleaner "${init_installation_path}${new_default_version}/bin/Rscript")" ${init_default_read_path}script
        

        # check if the change was successful
        if [ "$(func_R_symlink_health)" == "healthy" ]; then
            # show the user the final symlink status
            func_check_status
        else
            func_msg 1 error "We failed to set the default R version!"
        fi
    }
    
    
    function func_check_status () {
        ## Description:
        ##  It checks where the default R path is linked to and return a human
        ##   readable string
        ## 
        ## Arguments:
        ##  Does not get any input
        ## 
        ## Output:
        ##  it just returns a string value

        local symlink_status="$(func_R_symlink_health)"

        case "$symlink_status" in
        file_does_not_exists)
            func_msg 1 error "There is no file named '${init_default_read_path}'\n   Perhaps you have not ever set the R version.\n   Try using the \"change\" argument to set the version.";
            echo;
            exit 1;
            ;;
        file_not_symlink)
            func_msg 1 error "The file '${init_default_read_path}' is not a symlink!\n   Perhaps you have already installed R by other means.\n   Remove that and try installing through this script!";
            echo;
            exit 1;
            ;;
        symlink_broken)
            func_msg 1 error "The link of the '${init_default_read_path}' is broken!\n   Try using the \"change\" argument to set the version and fix this.";
            echo;
            exit 1;
            ;;
        healthy)
            #local R_symlink="${init_default_read_path} ---> $(readlink -f ${init_default_read_path})"
            #echo "${R_symlink}"
            func_msg 1 success "${init_default_read_path} ---> $(readlink -f ${init_default_read_path})";
            ;;
        *)
            func_msg 1 error 'Please report this: Unknown condition in func_check_status!';
        esac
    }
    
    
    function func_package_manager () {
        ## Description:
        ## a function to detect the package manager of the linux distro
        ##
        ## Arguments:
        ##  Does not get any input
        ## 
        ## Output:
        ##  it just returns a string value

        local pakacgemanager="unknown"
        command -v apt >/dev/null && pakacgemanager="apt"
        command -v pacman >/dev/null && pakacgemanager="pacman"

        # return the 
        echo ${pakacgemanager}
    }

    function func_install () {
        # get all available R versions based on the defined repo URL
        #available_versions=$(curl --silent ${init_R_source_repo_path} | grep 'href="R' | egrep -o 'R-[0-9](.[0-9])+')
        
        # store the current path to be returned to after installation
        before_installation_path=$(pwd)

        
        #-------[ check requirements and fix them ]-------#
        {
            # make sure the user has sudo access
            if [ "$(func_has_sudo)" == 'no_sudo' ]; then
                func_msg 1 error 'You need to have sudo access to install R'
                echo
                exit 1
            fi


            # make sure the init_installation_path is a directory and exists
            if [ ! -d "${init_installation_path}" ]; then
                func_msg 1 error "The folder '${init_installation_path}' that is suppose to contain installed versions of R does not exist!\n   Should we create it?"
                func_choice_of "Yes" "No"
                if [ ${selected_item} == "No" ]; then
                    func_msg 1 error 'We cannot proceed with installing R since the the defined path for installation does not exists, and script is not authorized to create it either.\n   Please either create it yourself or change the defined path in the begining of the script under the variable name `init_installation_path`.'
                    exit 1
                fi

                func_msg 2 message "Creating folder '${init_installation_path}'"
                sudo mkdir "${init_installation_path}"
                if [ ! -d ${init_installation_path} ]; then
                    func_msg 2 error "Failed to create the installation directory: '${init_installation_path}'"
                fi
            fi


            # make sure the libreadline is installed
            if [ ! -f '/usr/local/lib/libreadline.so' ] && \
               [ ! -f '/usr/lib/x86_64-linux-gnu/libreadline.so' ]; then
                
                func_msg 1 message 'The libreadline.so was not found. Should we attempt to install it?'
                func_choice_of "Yes" "No"
                if [ ${selected_item} == "No" ]; then
                    func_msg 1 error "We cannot proceed with installing R since this is a dependency. Please install it yourself."
                    exit 1
                fi
               
                # change the directory to the download tmp folder
                cd "${init_tmp_path}"
                
                # get the name of the latest readline version
                latest_readline_version=$(curl --silent 'ftp://ftp.gnu.org/gnu/readline/' | \
                                          grep "readline-[1-9].*.tar.gz$" | \
                                          awk '{print $9}' | \
                                          sort | \
                                          tail -n 1)
                
                func_msg 1 message "We are going to install ${latest_readline_version}"
                
                # download the latest 
                wget -c -N "https://ftp.gnu.org/gnu/readline/${latest_readline_version}"
                
                # extract the downloaded file
                tar -xzf ${latest_readline_version}
                
                # strip the file extension to get to the folder name that the source files have extracted to
                extracted_path=$(echo ${latest_readline_version} | awk -F '.tar' '{print $1}')
                
                cd "${extracted_path}/"
                
                # Build
                ./configure
                make
                sudo make install
                
                # safety check
                if [ ! -f '/usr/local/lib/libreadline.so' ] && \
                   [ ! -f '/usr/lib/x86_64-linux-gnu/libreadline.so' ]; then
                    func_msg 1 error 'Failed to install the readline. we are already in the downloaded location for you to investigate this further.'
                    exit 1
               else
                    func_msg 1 success "extracted_path installed successfully."
               fi
                
                # return to path before installation
                cd ${before_installation_path}
            fi


            # make sure LaTeX is installed (the installation of packages would fail since documentationsc cannot be parsed to PDF)
            if [ ! $(command -v pdflatex) ]; then
                func_msg 1 error 'LaTeX is not installed and it is needed for compiling R documentations.'
                func_msg 2 message 'Try installing LaTeX via'

                if [ "$(func_package_manager)" == "apt" ]; then
                    func_msg 3 message 'sudo apt update ; sudo apt install texlive-full libpcre2-dev'
                elif [ "$(func_package_manager)" == "pacman" ]; then
                    func_msg 3 message 'sudo pacman --refresh --refresh ; sudo pacman -S texlive-most'
                fi

                # exit the script
                exit 1
            fi
        }
        

        # get available R versions on the cloud
        available_versions=$(curl --silent ${init_R_source_repo_path} | \
                             grep 'href="R' | \
                             awk -F '"' '/R-[0-9](.[0-9])+/{print $2}')
        
        # ask user which one to be installed (the output will be in ${selected_item} variable
        func_msg 1 message 'Select the R version you wish to install by typing its index number:'
        func_choice_of ${available_versions}

        
        # extract the version number of selected R version
        local selected_version=$(echo ${selected_item} | sed -e 's/R-//g' -e 's/.tar.gz//')

        if [ -d "${init_installation_path}/${selected_version}" ]; then
            func_msg 1 message "The selected version is already installed at '$(func_path_cleaner "${init_installation_path}/${selected_version}")'"
            exit 1
        fi

        echo "|"
        func_msg 1 message "Downloading '${selected_item}' source file."
        
        # downlaod the file
        curl "${init_R_source_repo_path}/${selected_item}" --output "${init_tmp_path}/${selected_item}"
        
        # strip the file extension to get to the folder name that the source files have extracted to
        extracted_R_source_path=$(echo ${selected_item} | awk -F '.tar' '{print $1}')
        
        # make sure the installation path exist
        sudo mkdir -p ${init_installation_path}
        
        # change the directory to the download tmp folder
        cd "${init_tmp_path}"
        
        # extract the downloaded file
        tar --extract --gzip --file="${selected_item}" --directory="${init_tmp_path}"
        
        func_msg 1 success "The source was downloaded and extracted in '$(func_path_cleaner "${init_tmp_path}/${extracted_R_source_path}")'"

        # go inside the extracted folder
        cd ${extracted_R_source_path}

        func_msg 1 message "Installing dependencies for building the source"
        

        if [ "$(func_package_manager)" == "apt" ]; then
        
            # check if the sources.list file has any source lines that is commented out
            if [ $(cat '/etc/apt/sources.list' | grep '^# deb-src' | wc -l) -gt 0 ]; then
                func_msg 1 message 'There are some source lines in "/etc/apt/sources.list" that are commented. We will make a backup copy ("/etc/apt/sources.list.bk") and try to fix the original file.'
                
                # create a bckup from the sources.list file
                sudo cp '/etc/apt/sources.list' '/etc/apt/sources.list.bk'
                
                func_msg 2 success 'Backup was created.'
                
                # remove the cooemnts from all the source lines
                sudo sed -Ei 's/^# deb-src /deb-src /' '/etc/apt/sources.list'
                
                func_msg 2 success 'Lines were uncommented.'
            fi
            
            
            # get the dependencies
            sudo apt -qq update
            sudo apt -qq build-dep r-base

        elif [ "$(func_package_manager)" == "pacman" ]; then
            # get the dependencies and only install the packages that are not up-to-date or not installed
            sudo pacman -S --needed $(pacman -Si r | awk -F ":" '/Depends On/ { print $2}')

        fi
        
        
        func_msg 1 message "Building from source"

        # build the R from source
        ./configure --enable-jit --prefix="$(func_path_cleaner "${init_installation_path}/${selected_version}")" --enable-R-shlib --with-blas --with-lapack
        make
        sudo make install
        make install-info
        make install-pdf
        
        func_msg 1 success "Successfully installed!"

        # return to path before installation
        cd "${before_installation_path}"

        func_msg 1 message "Do you want this installed version (${selected_version}) to be the default?"
        func_choice_of "Yes" "No"
        if [ "${selected_item}" == "Yes" ]; then
            func_change_version ${selected_version}
        fi
        
        # clean up
        #rm -rf "${init_tmp_path}${selected_item}"
    }


    function func_uninstall () {

        #-------[ check requirements and fix them ]-------#
        {
            # make sure the user has sudo access
            if [ "$(has_sudo)" == 'no_sudo' ]; then
                func_msg 1 error 'You need to have sudo access to remove an installed version of R'
                echo
                exit 1
            fi
        }

        local installed_versions=$(func_list_installed)

        func_msg 1 message 'Which one these versions should be removed?'
        func_choice_of ${installed_versions}

        local to_be_removed="${selected_item}"

        echo "|"
        func_msg 1 message "Are you sure you want to remove R version '${to_be_removed}'?"
        func_choice_of "Yes" "No"
        if [ ${selected_item} == "No" ]; then
            func_msg 1 message "Removal of the R version '${to_be_removed}' is aborted!"
            exit 1
        elif [ ${selected_item} == "Yes" ]; then
            sudo rm -rf "${init_installation_path}/${to_be_removed}"

            if [ -d "${init_installation_path}/${to_be_removed}" ]; then
                func_msg 1 error "Failed to removed the selected R version!"
                exit 1
            fi

            func_msg 1 success "Removed!"

            # check if the symlink link is broken (perhaps it was linked to this version that we removed), suggest user to "change" the version
            if [ "$(func_R_symlink_health)" == "symlink_broken" ]; then
                func_msg 1 message "There is no working default R version. Do you want to set a working default?"
                func_choice_of "Yes" "No"
                if [ "${selected_item}" == "Yes" ]; then
                    func_change_version ${selected_version}
                fi
            fi
        fi
    }


    function func_has_sudo () {
        ## Description:
        ##  a function to check if the user has sudo access and if he has, does it need password
        ## 
        ## Arguments:
        ##  Does not get any input
        ## 
        ## Output:
        ##  it just returns a sting value
        ## 
        ## source:
        ##   https://superuser.com/a/1281228

        local prompt

        prompt=$(sudo -nv 2>&1)
        if [ $? -eq 0 ]; then
        echo "has_sudo__pass_set"
        elif echo $prompt | grep -q '^sudo:'; then
        echo "has_sudo__needs_pass"
        else
        echo "no_sudo"
        fi
    }


    function func_R_symlink_health () {
        ## Description:
        ##  a function to check if the file in init_default_read_path is healthy symlink
        ## 
        ## Arguments:
        ##  Does not get any input
        ## 
        ## Output:
        ##  it just returns a sting value of the status


        if [ ! -f "${init_default_read_path}" ]; then
            echo "file_does_not_exists"
        elif [ ! -L "${init_default_read_path}" ]; then
            echo "file_not_symlink"
        elif [ ! -e "${init_default_read_path}" ] ; then
            echo "symlink_broken"
        else
            echo "healthy"
        fi
    }


    function func_list_installed () {
        ## Description:
        ##  a function to list all the installed versions
        ## 
        ## Arguments:
        ##  Does not get any input
        ## 
        ## Output:
        ##  it just returns a sting value of the status

        local installed_versions=$(ls -1 ${init_installation_path})
        echo ${installed_versions}
    }


    function func_path_cleaner () {
        ## Description:
        ##  a function to remove extra / from given string
        ## 
        ## Arguments:
        ##  gets 1 argument
        ## 
        ## Output:
        ##  returns a sting value

        echo $1 | sed -E -e 's_/{2,}_/_'
    }
}


#-------[ checking the input ]-------#
{
    # check if arguments are provided
    if [ "$#" -eq "0" ]; then
        func_msg 1 error "No arguments supplied"
        echo
        func_manual
        exit 1
    fi
}


#------[ parsing arguments ]-------#
{
    echo
    case "${1}" in
        change)
            func_change_version;
            ;;
        check)
            func_check_status;
            ;;
        l|list)
            func_msg 1 success "The following versions are installed in '${init_installation_path}' :";
            #for i in $(ls -1 ${init_installation_path}); do func_msg 2 message ${i}; done;
            for i in $(func_list_installed); do func_msg 2 message ${i}; done;
            ;;
        i|install)
            func_install;
            ;;
        remove)
            func_uninstall;
            ;;
        h|help)
            func_manual;
            ;;
        *)
            func_manual;
            echo;
            exit 1;
            ;;
     esac
     echo
}

