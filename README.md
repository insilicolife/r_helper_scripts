# R helper scripts

This repository contains scripts to help to work with R easier. Enjoy them and create pull requests if you manage to improve them.

## Installation

Considering that these are just some scripts, there is not installation, but rather only having them on your machine would be suffice. Just clone this repo on your computer where ever you like, for example:

```sh
cd ~
git clone https://gitlab.com/m.mahmoudian/r_helper_scripts.git
```

The scripts should be already execulable, but in case they are not, you can use:

```sh
# navigate to the cloned folder
cd r_helper_scripts

# mark everything that ends with .sh as executable
chmod +x *.sh
```

## Usage

Simply try to run them from terminal:

```sh
r_helper_scripts/R.version.manager.sh
# or
bash r_helper_scripts/R.version.manager.sh
```

## Contribute

Checklist:

1. Make sure your code is commented and variable names are intuitive enough
2. Create pull request to have your contribution under your name and be acknowledged for them
